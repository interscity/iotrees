# IoTrees - Internet of Trees

This project aims at developing open source prototypes for monitoring the environment using free-design hardware and open source software. The first prototype has a sensing station and a base station. The sensing station is attached to a tree and sends the collected data to the base station, which is connected to the InterSCity Platform. The project is a partnership with the group managed by Prof. Marcos Buckeridge, from Institute of Biosciences of the University of São Paulo (IB/USP) in the context of the INCT InterSCity.

The reason for attaching the sensors to a tree is related to its importance to the environment. The health of the tree can be monitored through leaf transpiration, sap flow measurement, and environment evaluation. These methods offer indicators on any chance of a tree to fall down and also the level of humidity offered by the tree everyday to the cities.  If one considers only the trees within a city area it is possible to notice their important role in the ecosystem of a city due to its significant impact on reduction of air pollution, climate regulation, noise reduction, and other factors. On the other side, a tree that falls down can interrupt the traffic, destroy cars, and also cause an injury of a person that pass by this place inadvertently. This situation requires attention in order to take care of unhealthy trees on the streets and leverage the risk of problems. A better discussion about this problem is presented on the article “IoTrees: Sensing the city through its trees” (in Portuguese) published at Computação Brasil Magazine n. 37, 2018.

By now, the sensing station units offer:

- Light, Humidity, Temperature, Soil Moisture sensors;
- Measurement of  sap flow in plant stem;
- Leaf temperature.

## Monitoring stations
The stations were prototyped focusing on low-cost and open-design hardware considering that the main idea is to foster stations reproduction in large scale. The hardware used in these stations were:

### Sensing station:
- Arduino Nano microcontroller
- NRF24L01 radio transceiver
- DHT22 sensor
- LDR sensor
- Capacitive Soil Moisture sensor
- ADS1115 16Bit ADC 4 channel
- Type T thermocouple
- NiCr (heater)
- MOSFET IRF730

### Base station:
- NodeMCU CP2102 Module of ESP8266
- NRF24L01 radio transceiver

## Sensing station prototype
The latest hardware-design is presented below with some restrictions. The Type T thermocouple is represented by a thermistor, and the NiCr/Cu resistance is represented by a resistor due to Fritzing limitations. The schematic of this station is presented below:

![Sensing station schematics](IoTreesSensing/IoTreesSensing_bb.png?raw=true "Sensing station schematics")

The idea of using an Arduino Nano over a breadboard was intentional. This approach allows other sensors to be easily attached without modifying the sensing station in most of the cases.

The first prototype was created by Anne-Camille and Oumeima during their internship at Universidade de São Paulo. The second prototype was created by Max Rosan and Victor Seiji. The latest is just an upgrade of this second one, and was made by Antonio D de Carvalho J and Max Rosan.

## Base station prototype
The Base station works as a gateway for many sensing stations. The schematic of this station is presented below:

![Base station schematics](IoTreesBase/IoTreesBase_bb.png?raw=true "Base station schematics")

## Data monitoring
The data collected with the sensing station is sent to InterSCity platform and also saved on local computers or Micro SD cards. It is possible to view the real-time data at the [IoTrees Dashboard](http://iotrees.interscity.org/client.html).
